package main

import (
	"flag"

	"seabattle/conf"
	"seabattle/controller"
)

func main() {
	path := flag.String("path", "./conf/config.json", "Path to config file")
	flag.Parse()
	config := conf.NewFromFile(*path)

	c := controller.New(config)
	c.ServeHTTP()
}
