package conf

import (
	"encoding/json"
	"io/ioutil"

	"github.com/sirupsen/logrus"
)

// Main stores application settings
type Main struct {
	Port int `json:"port"`
}

// NewFromFile creates new conf from selected file
func NewFromFile(path string) *Main {
	configJSON, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.WithError(err).WithField("path to file", path).Panic("Can't read config file")
	}
	var c Main
	if err := json.Unmarshal(configJSON, &c); err != nil {
		logrus.WithError(err).WithField("path to file", path).Panic("Can't unmarshal config")
	}
	return &c
}
