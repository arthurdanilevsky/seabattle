package controller

import "testing"

func Test_validateShips(t *testing.T) {
	tests := []struct {
		name string
		args string
		want bool
	}{
		{name: "true-1", args: "1A 2B", want: true},
		{name: "true-2", args: "1A 2B,3D 4C", want: true},
		{name: "true-3", args: "1A 2B,3D 4C,26D 20Z", want: true},
		{name: "false-1", args: "1A", want: false},
		{name: "false-2", args: "1A2B,3D4C", want: false},
		{name: "false-3", args: "1A 2B 3D 4C", want: false},
		{name: "false-4", args: "", want: false},
		{name: "false-5", args: "1a 2B,3D 4C,26D 20Z", want: false},
		{name: "false-6", args: "0A 2B,3D 4C,26D 20Z", want: false},
		{name: "false-7", args: "A1 2B,3D 4C", want: false},
		{name: "false-8", args: "27A 2B,3D 4C", want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := validateShips(tt.args); got != tt.want {
				t.Errorf("validateShips(\"%s\") = %v, want %v", tt.args, got, tt.want)
			}
		})
	}
}
