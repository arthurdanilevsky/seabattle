package controller

import (
	"errors"
	"regexp"

	"github.com/gin-gonic/gin"
)

const ok = "ok"

func (c *Controller) initBoard(ctx *gin.Context) {
	var req struct {
		Range int `json:"range" binding:"min=1,max=26"`
	}
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		c.respondError(ctx, err)
		return
	}
	err = c.app.InitBoard(req.Range)
	if err != nil {
		c.respondError(ctx, err)
		return
	}
	c.respondOK(ctx, ok)
}

func (c *Controller) initShips(ctx *gin.Context) {
	var req struct {
		Coordinates string `json:"coordinates" binding:"required"`
	}
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		c.respondError(ctx, err)
		return
	}
	if !validateShips(req.Coordinates) {
		c.respondError(ctx, errors.New("invalid ship coordinates format"))
		return
	}

	err = c.app.InitShips(req.Coordinates)
	if err != nil {
		c.respondError(ctx, err)
		return
	}

	c.respondOK(ctx, ok)
}

func validateShips(s string) bool {
	template := `^(([1-9]|1[0-9]|2[0-6])[A-Z]\ ([1-9]|1[0-9]|2[0-6])[A-Z])(,(([1-9]|1[0-9]|2[0-6])[A-Z]\ ([1-9]|1[0-9]|2[0-6])[A-Z]))*$`
	ok, _ := regexp.MatchString(template, s)
	return ok
}

func (c *Controller) shot(ctx *gin.Context) {
	var req struct {
		Coord string `json:"coord" binding:"required"`
	}
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		c.respondError(ctx, err)
		return
	}
	res, err := c.app.Shot(req.Coord)
	if err != nil {
		c.respondError(ctx, err)
		return
	}
	c.respondOK(ctx, res)
}

func (c *Controller) clearGame(ctx *gin.Context) {
	c.app.Clear()
	c.respondOK(ctx, ok)
}

func (c *Controller) getState(ctx *gin.Context) {
	ships, knocked, destroyed, shots, err := c.app.State()
	if err != nil {
		c.respondError(ctx, err)
		return
	}
	c.respondOK(ctx, map[string]int{
		"ship_count": ships,
		"knocked":    knocked,
		"destroyed":  destroyed,
		"shot_count": shots,
	})
}
