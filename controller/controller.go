package controller

import (
	"fmt"
	"net/http"

	"seabattle/application"
	"seabattle/conf"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// Controller is presentation tier of 3-layer architecture (2 layers in our case)
type Controller struct {
	config *conf.Main
	app    *application.Application
	router *gin.Engine
}

// New returns new Controller
func New(config *conf.Main) Controller {
	return Controller{
		config: config,
		app:    application.New(),
		router: gin.Default(),
	}
}

// ServeHTTP starts http server
func (c *Controller) ServeHTTP() {
	c.initRoutes()

	srv := &http.Server{
		Addr:    fmt.Sprint(":", c.config.Port),
		Handler: c.router,
	}

	err := srv.ListenAndServe()
	if err != http.ErrServerClosed {
		logrus.WithError(err).Fatal("can't start http server")
	}
}

func (c *Controller) initRoutes() {
	c.router.POST("/create-matrix", c.initBoard)
	c.router.POST("/ship", c.initShips)
	c.router.POST("/shot", c.shot)
	c.router.POST("/clear", c.clearGame)
	c.router.GET("/state", c.getState)
}

func (c *Controller) respondError(ctx *gin.Context, err error) {
	h := gin.H{"error": err.Error()}
	ctx.JSON(http.StatusBadRequest, h)
}

func (c *Controller) respondOK(ctx *gin.Context, result interface{}) {
	ctx.JSON(http.StatusOK, gin.H{"result": result})
}
