package application

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_parsePoint(t *testing.T) {
	app := New()
	tests := []struct {
		name    string
		args    string
		want    Point
		wantErr error
	}{
		{name: "positive-1", args: "1A", want: Point{0, 0}, wantErr: nil},
		{name: "positive-2", args: "26Z", want: Point{25, 25}, wantErr: nil},
		{name: "positive-3", args: "2D", want: Point{1, 3}, wantErr: nil},
		{name: "negative-1", args: "DD", want: Point{}, wantErr: errParsePoint},
		{name: "negative-2", args: "11", want: Point{}, wantErr: errParsePoint},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := app.parsePoint(tt.args); got != tt.want || err != tt.wantErr {
				t.Errorf("parsePoint(\"%s\") = (%v, %v), want (%v, %v)", tt.args, got, err, tt.want, tt.wantErr)
			}
		})
	}
}

func Test_validateShipCoord(t *testing.T) {
	app := New()
	app.size = 6
	tests := []struct {
		name string
		args ShipCoord
		want error
	}{
		// Board borders tests
		{name: "positive-0/inside", args: ShipCoord{Point{2, 2}, Point{3, 3}}, want: nil},

		{name: "positive-1/tl-corner", args: ShipCoord{Point{0, 0}, Point{0, 0}}, want: nil},
		{name: "positive-2/tr-corner", args: ShipCoord{Point{0, 5}, Point{0, 5}}, want: nil},
		{name: "positive-3/br-corner", args: ShipCoord{Point{5, 5}, Point{5, 5}}, want: nil},
		{name: "positive-4/bl-corner", args: ShipCoord{Point{5, 0}, Point{5, 0}}, want: nil},

		{name: "positive-5/t-side", args: ShipCoord{Point{0, 2}, Point{0, 3}}, want: nil},
		{name: "positive-6/r-side", args: ShipCoord{Point{2, 5}, Point{3, 5}}, want: nil},
		{name: "positive-7/b-side", args: ShipCoord{Point{5, 2}, Point{5, 3}}, want: nil},
		{name: "positive-8/l-side", args: ShipCoord{Point{0, 2}, Point{0, 3}}, want: nil},

		{name: "negative-0/outside", args: ShipCoord{Point{6, 6}, Point{7, 7}}, want: errShipOutOfBoard},

		{name: "negative-1/tl-corner", args: ShipCoord{Point{-1, -1}, Point{0, 0}}, want: errShipOutOfBoard},
		{name: "negative-2/tr-corner", args: ShipCoord{Point{-1, 5}, Point{0, 6}}, want: errShipOutOfBoard},
		{name: "negative-3/br-corner", args: ShipCoord{Point{5, 5}, Point{6, 6}}, want: errShipOutOfBoard},
		{name: "negative-4/bl-corner", args: ShipCoord{Point{5, -1}, Point{6, 0}}, want: errShipOutOfBoard},

		{name: "negative-5/t-side", args: ShipCoord{Point{-1, 2}, Point{0, 3}}, want: errShipOutOfBoard},
		{name: "negative-6/r-side", args: ShipCoord{Point{2, 5}, Point{3, 6}}, want: errShipOutOfBoard},
		{name: "negative-7/b-side", args: ShipCoord{Point{5, 2}, Point{6, 3}}, want: errShipOutOfBoard},
		{name: "negative-8/l-side", args: ShipCoord{Point{-1, 2}, Point{0, 3}}, want: errShipOutOfBoard},

		// Relative position of ship corners test
		{name: "negative-9/relative", args: ShipCoord{Point{2, 2}, Point{0, 0}}, want: errInvalidShipCorners},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := app.validateShipCoord(tt.args); err != tt.want {
				t.Errorf("validateShipCoord(%+v) = %v, want %v", tt.args, err, tt.want)
			}
		})
	}
}

func Test_parseShips(t *testing.T) {
	app := New()
	app.size = 9
	tests := []struct {
		name    string
		args    string
		want    []ShipCoord
		wantErr error
	}{
		{name: "positive-1", args: "1A 2B,4C 5D,7A 8E", want: []ShipCoord{
			{Point{0, 0}, Point{1, 1}},
			{Point{3, 2}, Point{4, 3}},
			{Point{6, 0}, Point{7, 4}},
		}, wantErr: nil},
		{name: "positive-2", args: "1A 2B", want: []ShipCoord{{Point{0, 0}, Point{1, 1}}}, wantErr: nil},
		{name: "negative-1", args: "1A2B", want: nil, wantErr: errSplitShipCoordinates},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := app.parseShips(tt.args); !reflect.DeepEqual(got, tt.want) || err != tt.wantErr {
				t.Errorf("parseShips(\"%s\") = (%v, %v), want (%v, %v)", tt.args, got, err, tt.want, tt.wantErr)
			}
		})
	}
}

func Test_addShip_OK(t *testing.T) {
	// Asset
	// app with one ship
	app := New()
	err := app.InitBoard(6)
	require.Nil(t, err)

	ship := Ship{}
	app.board[1][1].ship = &ship
	app.board[1][2].ship = &ship
	for i := 0; i <= 2; i++ {
		for j := 0; j <= 3; j++ {
			app.board[i][j].reserved = true
		}
	}
	ship.cells = []*Cell{&app.board[1][1], &app.board[1][2]}

	app.ships = []Ship{ship}

	// app with 2 ships
	app2 := New()
	err = app2.InitBoard(6)
	require.Nil(t, err)

	ship1 := Ship{}
	app2.board[1][1].ship = &ship1
	app2.board[1][2].ship = &ship1
	for i := 0; i <= 2; i++ {
		for j := 0; j <= 3; j++ {
			app2.board[i][j].reserved = true
		}
	}
	ship1.cells = []*Cell{&app2.board[1][1], &app2.board[1][2]}

	ship2 := Ship{}
	app2.board[4][4].ship = &ship2
	for i := 3; i <= 5; i++ {
		for j := 3; j <= 5; j++ {
			app2.board[i][j].reserved = true
		}
	}
	ship2.cells = []*Cell{&app2.board[4][4]}

	app2.ships = []Ship{ship1, ship2}

	// Act
	// add another ship to app and compare with app2
	sc := ShipCoord{Point{4, 4}, Point{4, 4}}
	err = app.validateShipCoord(sc)
	require.Nil(t, err)
	err = app.addShip(sc)

	// Arrange
	require.Nil(t, err)
	require.Equal(t, app2, app)
}

func Test_addShip_Err(t *testing.T) {
	// Asset
	// app with one ship
	app := New()
	err := app.InitBoard(6)
	require.Nil(t, err)

	ship := Ship{}
	app.board[1][1].ship = &ship
	app.board[1][2].ship = &ship
	for i := 0; i <= 2; i++ {
		for j := 0; j <= 3; j++ {
			app.board[i][j].reserved = true
		}
	}
	ship.cells = []*Cell{&app.board[1][1], &app.board[1][2]}

	app.ships = []Ship{ship}

	// Act
	// try add another ship with intersects to app
	sc := ShipCoord{Point{2, 3}, Point{2, 3}}
	err = app.validateShipCoord(sc)
	require.Nil(t, err)
	err = app.addShip(sc)

	// Arrange
	require.Equal(t, errShipsIntersection, err)
}

func Test_validatePoint(t *testing.T) {
	app := New()
	app.size = 4
	tests := []struct {
		name string
		args Point
		want bool
	}{
		{name: "positive-1", args: Point{0, 0}, want: true},
		{name: "positive-2", args: Point{3, 3}, want: true},
		{name: "negative-1", args: Point{4, 4}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := app.validatePoint(tt.args); got != tt.want {
				t.Errorf("validatePoint(%v) = %v, want %v", tt.args, got, tt.want)
			}
		})
	}
}
