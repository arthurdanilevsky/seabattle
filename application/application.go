package application

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

var (
	errParsePoint             = errors.New("can't parse ship corner coordinates")
	errShipOutOfBoard         = errors.New("ship coordinates are out of board")
	errInvalidShipCorners     = errors.New("invalid ship corners coordinates")
	errSplitShipCoordinates   = errors.New("can't split ship coordinates")
	errShipsIntersection      = errors.New("some ships have intersects or too close to each other")
	errBoardIsNotInitialized  = errors.New("board is not initialized")
	errShipsAreNotInitialized = errors.New("ships are not initialized")
)

// Application is sea-battle game representation
type Application struct {
	size  int
	board [][]Cell
	ships []Ship
}

// New returns new application
func New() *Application {
	return &Application{}
}

// Cell is cell of board representation
type Cell struct {
	reserved bool
	shot     bool
	ship     *Ship
}

// Ship is ship representation
type Ship struct {
	cells []*Cell
}

func (s *Ship) knocked() bool {
	for _, c := range s.cells {
		if c == nil {
			logrus.WithField("ship", fmt.Sprintf("%+v", s)).Panic("ship has nil cell")
		}
		if c.shot {
			return true
		}
	}
	return false
}

func (s *Ship) destroyed() bool {
	for _, c := range s.cells {
		if c == nil {
			logrus.WithField("ship", fmt.Sprintf("%+v", s)).Panic("ship has nil cell")
		}
		if !c.shot {
			return false
		}
	}
	return true
}

func newBoard(size int) [][]Cell {
	res := make([][]Cell, size)
	for i := range res {
		res[i] = make([]Cell, size)
	}
	return res
}

// InitBoard initializes new game board
func (a *Application) InitBoard(size int) error {
	if a.size > 0 {
		return errors.New("board already initialized")
	}
	a.size = size
	a.board = newBoard(size)
	return nil
}

// InitShips inits ships
func (a *Application) InitShips(coordinates string) error {
	if a.size == 0 {
		return errBoardIsNotInitialized
	}
	if !a.shipsDestroyed() {
		return errors.New("not all ships are destroyed")
	}
	shipsCoords, err := a.parseShips(coordinates)
	if err != nil {
		return err
	}

	a.ships = make([]Ship, 0)
	for _, sc := range shipsCoords {
		err = a.addShip(sc)
		if err != nil {
			a.ships = nil
			a.board = newBoard(a.size) // re-init board cells
			return err
		}
	}
	return nil
}

// Point is board cell coordinates
type Point struct {
	x int
	y int
}

// ShipCoord is top left and bottom right corners of ship on the board
type ShipCoord struct {
	topLeft     Point
	bottomRight Point
}

// parseShips parses strings looking like "1A 2B,4C 5D,7A 8E" to slice of type ShipCoord:
// [{{0 0} {1 1}} {{3 2} {4 3}} {{6 0} {7 4}}]
// in another words transforms human-readable coordinates of ships to coordinates of board cells (indices of 2D-slice)
func (a *Application) parseShips(coordinates string) ([]ShipCoord, error) {
	var err error
	shipStr := strings.Split(coordinates, ",")
	res := make([]ShipCoord, len(shipStr))
	for i, s := range shipStr {
		corners := strings.Split(s, " ")
		if len(corners) != 2 {
			return nil, errSplitShipCoordinates
		}

		res[i].topLeft, err = a.parsePoint(corners[0])
		if err != nil {
			return nil, err
		}
		res[i].bottomRight, err = a.parsePoint(corners[1])
		if err != nil {
			return nil, err
		}

		err := a.validateShipCoord(res[i])
		if err != nil {
			return nil, err
		}
	}
	return res, nil
}

// parsePoint parses coordinates of ship corner
// "4E" -> {3, 4}
func (a *Application) parsePoint(s string) (Point, error) {
	reg := regexp.MustCompile("[0-9]+")
	xStr := reg.FindAllString(s, 1)
	if len(xStr) != 1 {
		return Point{}, errParsePoint
	}
	x, err := strconv.Atoi(xStr[0])
	if err != nil {
		return Point{}, err
	}

	reg = regexp.MustCompile("[A-Z]")
	yStr := reg.FindAllString(s, 1)
	if len(yStr) != 1 {
		return Point{}, errParsePoint
	}
	y := int([]byte(yStr[0])[0] - byte('A'))

	return Point{
		x: x - 1,
		y: y,
	}, nil
}

// validateShipCoord checks that:
// - ship corners are inside board
// - ship corners coordinates are valid
func (a *Application) validateShipCoord(sc ShipCoord) error {
	// Check board borders and ship coordinates
	ok := a.validatePoint(sc.topLeft) && a.validatePoint(sc.bottomRight)
	if !ok {
		return errShipOutOfBoard
	}
	// Check top left and bottom right corners relative position
	ok = sc.topLeft.x <= sc.bottomRight.x && sc.topLeft.y <= sc.bottomRight.y
	if !ok {
		return errInvalidShipCorners
	}
	return nil
}

// validatePoint checks that point is inside board
func (a *Application) validatePoint(p Point) bool {
	return p.x < a.size && p.x >= 0 && p.y < a.size && p.y >= 0
}

// addShip adds ship on board:
// marks some cells as added to ship
// add this ship to ships list
// Note, that ship coordinates must be validated before call this method and game must be initialized
func (a *Application) addShip(sc ShipCoord) error {
	// (x1, x1), (x2, y2) - coordinates of "extended" ship - ship and area around it (perimeter with width = one cell)
	x1, y1, x2, y2 := sc.topLeft.x-1, sc.topLeft.y-1, sc.bottomRight.x+1, sc.bottomRight.y+1
	var ship Ship
	for i := x1; i <= x2; i++ {
		if i < 0 || i > a.size-1 { // check board borders
			continue
		}
		for j := y1; j <= y2; j++ {
			if j < 0 || j > a.size-1 { // check board borders
				continue
			}
			if i == x1 || i == x2 || j == y1 || j == y2 { // mark area around the ship as reserved
				a.board[i][j].reserved = true
				continue
			}
			if a.board[i][j].reserved {
				return errShipsIntersection
			}
			a.board[i][j].ship = &ship
			a.board[i][j].reserved = true
			ship.cells = append(ship.cells, &a.board[i][j])
		}
	}
	a.ships = append(a.ships, ship)
	return nil
}

// ShotRes is result of the shoot
type ShotRes struct {
	Destroy bool `json:"destroy"`
	Knock   bool `json:"knock"`
	End     bool `json:"end"`
}

// Shot shots cell and return some statistics
func (a *Application) Shot(coord string) (ShotRes, error) {
	if a.size == 0 {
		return ShotRes{}, errBoardIsNotInitialized
	}
	if len(a.ships) == 0 {
		return ShotRes{}, errShipsAreNotInitialized
	}

	p, err := a.parsePoint(coord)
	if err != nil {
		return ShotRes{}, err
	}
	if !a.validatePoint(p) {
		return ShotRes{}, errors.New("point coordinates are out of board")
	}

	if a.shipsDestroyed() {
		return ShotRes{}, errors.New("can't shot, all ships already destroyed")
	}

	if a.board[p.x][p.y].shot {
		return ShotRes{}, errors.New("point already shot")
	}

	var res ShotRes
	a.board[p.x][p.y].shot = true
	if a.board[p.x][p.y].ship != nil {
		res.Knock = true
		if a.board[p.x][p.y].ship.destroyed() {
			res.Destroy = true
			res.End = a.shipsDestroyed()
		}
	}

	return res, nil
}

// shipsDestroyed checks if all ships are destroyed
func (a *Application) shipsDestroyed() bool {
	for _, s := range a.ships {
		if !s.destroyed() {
			return false
		}
	}
	return true
}

// Clear clears game state
func (a *Application) Clear() {
	a.size = 0
	a.board = nil
	a.ships = nil
}

// State returns count of ships, count of knocked and count of destroyed ships, total count of made shots
func (a *Application) State() (int, int, int, int, error) {
	if a.size == 0 {
		return 0, 0, 0, 0, errBoardIsNotInitialized
	}
	if len(a.ships) == 0 {
		return 0, 0, 0, 0, errShipsAreNotInitialized
	}

	var knocked, destroyed, shots int

	for _, row := range a.board {
		for _, c := range row {
			if c.shot {
				shots++
			}
		}
	}

	for _, s := range a.ships {
		if s.destroyed() {
			destroyed++
			continue
		}
		if s.knocked() {
			knocked++
		}
	}

	printBoard(a.board)
	return len(a.ships), knocked, destroyed, shots, nil
}

func printBoard(board [][]Cell) {
	size := len(board)
	var str string
	for i := -1; i < size; i++ {
		for j := -1; j < size; j++ {
			switch {
			case i < 0 && j < 0:
				str = "  "
			case i < 0 && j >= 0:
				str = string(byte(j)+byte('A')) + " "
			case i >= 0 && j < 0:
				str = string(byte('1')+byte(i)) + " "
			default:
				str = "◻ "
				if board[i][j].shot {
					str = "● "
				}
				if board[i][j].ship != nil {
					str = "◇ "
					if board[i][j].shot {
						str = "◆ "
					}
				}
			}
			fmt.Print(str)
		}
		fmt.Println()
	}
}
